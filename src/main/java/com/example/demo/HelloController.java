package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	
	@GetMapping("/hello/name")
	public String hello(@PathVariable("name") String name) {
		return "hello "+name;
	}
	
	
	@GetMapping("/hello1")
	public String name() {
		return "hello ji";
	}
	
	
	@GetMapping("/welcome")
	public String welcomeMessage() {
		return "you are most welcome";
	}
	
	
	@GetMapping("/welcome1")
	public String welcomeMessage1() {
		return "you are most welcome1";
	}
}
